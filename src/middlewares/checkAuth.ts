import jwtDecode from 'jwt-decode'
import 'dotenv/config'
import { UNAUTHORIZED } from '../constants/statusCodes'

const { COWSOKO_ISS } = process.env
interface DecodedInterface {
  iss: string
  iat: string | number
  exp: string | number
  aud: string
  admin: string
  sub: any
  account_type: string
}

const checkAuth = async (req: any, res: any, next: any) => {
  const { authorization = '' } = req.headers
  const token = authorization.slice(7)

  if (!token) {
    return res.status(UNAUTHORIZED).json({ status: UNAUTHORIZED, message: 'Unauthorized access', reason: 'VALIDATION' })
  }

  const decoded: DecodedInterface = jwtDecode(token)

  if (!decoded || decoded.iss !== COWSOKO_ISS) {
    return res.status(UNAUTHORIZED).json({ status: UNAUTHORIZED, message: 'Unauthorized access', reason: 'ORIGIN' })
  }

  req.currentUser = { id: decoded.sub, admin: decoded.admin, account_type: decoded.account_type }
  return next()
}

export const checkIfAdmin = async (req: any, res: any, next: any) => {
  const {
    currentUser: { admin },
  } = req
  if (!admin) {
    return res
      .status(UNAUTHORIZED)
      .json({ status: UNAUTHORIZED, message: 'Unauthorized access', reason: 'ACCESS_LEVEL' })
  }
  return next()
}

export default checkAuth
