import fs from 'fs'
import path from 'path'
import { expect } from 'chai'
import ReceiptService from '../ReceiptService'
import { compileToTemplate, buildPDF } from '../../utils/PdfService'

describe('Functionality for generating Receipts', () => {
  it('Compiles the data with the HTML template', async () => {
    try {
      const RECIEPT = new ReceiptService('default.hbs')
      await RECIEPT.compile({ testname: 'compileToTemplate' })
    } catch (error) {
      expect(error.message).to.equal('Error: Error: "invoiceNumber" not defined in [object Object]')
    }
  })

  it('Successfully compiles a template', async () => {
    const template = await compileToTemplate(
      await fs.readFileSync(path.resolve(__dirname, `./__mocks__/test.hbs`), 'utf-8'),
      { testname: 'compileToTemplate' },
    )
    expect(template).to.be.a('string')
  })
})
