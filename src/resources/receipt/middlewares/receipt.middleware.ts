import jsonResponse from '../../../utils/jsonResponse'
import { FORBIDDEN } from '../../../constants/statusCodes'

/**
 * Checks if invoice has been paid
 * @param {any} req
 * @param {any} res
 * @param {*} next
 * @returns {*} response
 */
export const checkIfInvoicePaid = async (req: any, res: any, next: any) => {
  const { invoice } = req
  if (invoice.paymentStatus !== 'paid') {
    return jsonResponse({
      res,
      status: FORBIDDEN,
      message:
        'The invoice associated with this receipt is still pending, You will not be allowed access to a receipt.',
    })
  }
  return next()
}
