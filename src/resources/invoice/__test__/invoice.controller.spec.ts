import chai, { expect } from 'chai'
import chaiHttp from 'chai-http'
import { mockReq, mockRes } from 'sinon-express-mock'

import db from '../../../database/models'

import InvoiceController from '../invoice.controller'
import { createOrder } from './../../order/__tests__/__mocks__/data'
import { OK, NOT_FOUND } from '../../../constants/statusCodes'
import sinon from 'sinon'

const Controller: InvoiceController = new InvoiceController()

chai.use(chaiHttp)

let invoiceId: string | undefined

let order: any

describe('invoices endpoints', () => {
  before(async () => {
    order = await db.Order.create(createOrder)
  })
  beforeEach(async () => {
    const invoice = await db.Invoice.create({
      userId: 2298,
      invoiceNumber: '#2567',
      amountToBePaid: 5000,
      orderId: order.id,
      paymentStatus: 'pending',
    })
    invoiceId = invoice.id
  })

  it('should be able to view all the invoices in the system', done => {
    const request = mockReq({
      currentUser: { id: 2298 },
    })
    const response = mockRes()

    Controller.getMany(request, response).then(() => {
      expect(response.status).to.have.been.calledWith(OK)
      done()
    })
  })

  it('should return a single invoice record', done => {
    const request = mockReq({
      currentUser: { id: 2298 },
      params: {
        id: invoiceId,
      },
    })
    const response = mockRes()

    Controller.getRecord(request, response).then(() => {
      expect(response.status).to.have.been.calledWith(OK)
      done()
    })
  })

  it('should check if an invoice record exists & pass', done => {
    const request = mockReq({
      currentUser: { id: 2298 },
      params: {
        id: invoiceId,
      },
    })
    const response = mockRes()
    const next = sinon.spy()

    Controller.checkRecord(request, response, next).then(() => {
      expect(next.calledOnce).to.be.true
      done()
    })
  })

  it('should check if an invoice record exists & fail', done => {
    const request = mockReq({
      currentUser: { id: 2000 },
      params: {
        id: invoiceId,
      },
    })
    const response = mockRes()
    const next = sinon.spy()

    Controller.checkRecord(request, response, next).then(() => {
      expect(response.status).to.have.been.calledWith(NOT_FOUND)
      done()
    })
  })

  it('should successfully update an invoice record', done => {
    const request = mockReq({
      currentUser: { id: 2298 },
      body: { paymentStatus: 'paid' },
      params: {
        id: invoiceId,
      },
    })
    const response = mockRes()

    Controller.updateInvoice(request, response).then(() => {
      expect(response.status).to.have.been.calledWith(OK)
      done()
    })
  })
})
