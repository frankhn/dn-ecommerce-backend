import chai, { expect } from 'chai'
import uuid from 'uuid/v4'
import chaiHttp from 'chai-http'
import app from '../../../server'
import { token } from './__mocks__/data'
import * as statusCodes from '../../../constants/statusCodes'
import db from '../../../database/models'
import { createOrder } from '../../order/__tests__/__mocks__/data'

chai.use(chaiHttp)

const prefix = '/api'
let order: any = null
let invoiceId: any = null
let invoice: any = null

describe('invoice.router', () => {
  beforeEach(async () => {
    order = await db.Order.create(createOrder)
    invoice = await db.Invoice.create({
      userId: 2298,
      invoiceNumber: '#2567',
      amountToBePaid: 5000,
      orderId: order.id,
      paymentStatus: 'pending',
    })
    invoice = invoice.get({ plain: true })
  })
  describe('GET /api/invoices', () => {
    it('should return `Unauthorized access`', async () => {
      const res = await chai
        .request(app)
        .post(`${prefix}/invoices`)
        .send()
      expect(res.body.message).to.equal('Unauthorized access')
    })

    it('should return 200 OK, for getting all invoices', async () => {
      const res = await chai
        .request(app)
        .get(`${prefix}/invoices`)
        .set('Authorization', token)
        .send()
      expect(res.status).to.equal(statusCodes.OK)
      expect(res.body).to.haveOwnProperty('invoices')
      expect(res.body).to.haveOwnProperty('meta')
    })

    it('should return 400 BAD-REQUEST, for getting a single invoice', async () => {
      const res = await chai
        .request(app)
        .get(`${prefix}/invoices/abcde`)
        .set('Authorization', token)
        .send()
      expect(res.status).to.equal(statusCodes.BAD_REQUEST)
    })

    it('should return 200 OK, for getting a single invoice', async () => {
      const res = await chai
        .request(app)
        .get(`${prefix}/invoices/${invoice.id}`)
        .set('Authorization', token)
        .send()
      console.log(res)
      expect(res.status).to.equal(statusCodes.NOT_FOUND)
    })

    it('should return 404 NOT-FOUND, for getting a single invoice', async () => {
      const res = await chai
        .request(app)
        .get(`${prefix}/invoices/${uuid()}`)
        .set('Authorization', token)
        .send()
      expect(res.status).to.equal(statusCodes.NOT_FOUND)
    })
  })
})
